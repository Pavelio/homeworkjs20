"use strict";
function deadline(developer, backlog, deadL) {
    let sumDevelop = 0, sumBacklog = 0;
    for(let i = 0; i<developer.length; i++) {
        sumDevelop+=developer[i];
    }
    for(let i = 0; i<backlog.length; i++) {
        sumBacklog+=backlog[i];
    }
    let needHoursWork = (sumBacklog/sumDevelop)*8;
    let now = new Date();
    let workDayHave = 0;
    for(let i = now; i <= deadL; i= new Date(i.setDate(i.getDate() + 1))) {
      if(( i.getDay() >0) && ( i.getDay() <6)) {
        workDayHave++;
      }
    }
    if(now.getHours()>=18) {
        workDayHave--;
    }
    let workHoursHave = workDayHave*8;
    if((now.getHours()>10) && (now.getHours()<18)) {
        workHoursHave-= 8-(18 - now.getHours());
    }
    let checkDeadline = workHoursHave-needHoursWork;
    
    if(checkDeadline>=0) {
        let day = needHoursWork/8;
        console.log(`Все задачи будут успешно выполнены за ${day} дней, до наступления дедлайна!`);
    }
    else {
        checkDeadline = parseInt(-checkDeadline);
        console.log(`Команде разработчиков придется потратить дополнительно ${checkDeadline} часов после дедлайна, чтобы выполнить все задачи в беклоге`);
    }
};
let developerPoint = [4, 7, 6, 5, 9];
let backlogPoint = [25, 6, 23, 31, 42, 21];
let deadLine = new Date (2019, 3, 29, 23, 59);
deadline(developerPoint, backlogPoint, deadLine);